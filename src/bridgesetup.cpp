// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

#include "bridgesetup.h"

#include <QString>
#include <QFutureWatcher>
#include <QVector>
#include <QSettings>
#include <QStringBuilder>
#include "qtgbot/api.h"
#include "qtgbot/sendmessage.h"
#include "qtgbot/message.h"
#include "qtgbot/chat.h"
#include "database.h"
#include "tgbotclient.h"
#include "mucservice.h"
#include "global.h"

// as long as we can't use c++20... :'(
#define iter(variable) variable.begin(), variable.end()
#define iterc(variable) variable.cbegin(), variable.cend()

enum BridgeAction : uint8_t {
    NoAction,
    CreateBridgeAction,
    DestroyBridgeAction,
};

struct SetupState
{
    MucJid mucJid;
    int64_t tgChatId;
    bool tgAccepted;
    bool xmppAccepted;
    bool xmppReadyToAccept;

    bool operator==(const MucJid &jid) const
    {
        return mucJid == jid;
    }
    bool operator==(int64_t chatId) const
    {
        return tgChatId == chatId;
    }
    bool operator==(const std::variant<MucJid, int64_t> &variant) const
    {
        return std::holds_alternative<MucJid>(variant)
                ? mucJid   == std::get<MucJid >(variant)
                : tgChatId == std::get<int64_t>(variant);
    }
    bool operator==(const std::pair<MucJid, int64_t> &pair) const
    {
        return *this == pair.first || *this == pair.second;
    }

    bool accepted() const
    {
        return xmppReadyToAccept && tgAccepted && xmppAccepted;
    }
};

struct BridgeSetupPrivate
{
    BridgeSetupPrivate(Database *database, QVector<DbGroup> &groups)
        : database(database), groups(groups)
    {
    }

    Database *database;
    MucService *mucClient = nullptr;
    TgBotClient *tgClient = nullptr;
    QVector<SetupState> states;
    QVector<DbGroup> &groups;
    QSet<MucJid> bridgeCancellations;
};

BridgeSetup::BridgeSetup(Database *database, QVector<DbGroup> &groups, QObject *parent)
    : QObject(parent),
      d(new BridgeSetupPrivate(database, groups))
{
}

BridgeSetup::~BridgeSetup() = default;

void BridgeSetup::setMucClient(MucService *mucClient)
{
    d->mucClient = mucClient;
}

void BridgeSetup::setTgClient(TgBotClient *tgClient)
{
    d->tgClient = tgClient;
}

void BridgeSetup::addSetup(const MucJid &mucJid, int64_t tgChatId)
{
    if (std::find(iterc(d->states), std::pair(mucJid, tgChatId)) != d->states.cend()) {
        d->mucClient->sendBotMessage(mucJid, QStringLiteral("There's already an active setup for this channel. You can abort it with `/tg abort`."));
        return;
    }

    if (std::find(iterc(d->groups), std::pair(QString(mucJid), tgChatId)) != d->groups.cend()) {
        d->mucClient->sendBotMessage(mucJid, QStringLiteral("There's already a bridge set up in this channel or the desired Telegram group."));
        return;
    }

    d->states << SetupState {
        mucJid,
        tgChatId,
        false,
        false,
        false,
    };

    const QTgBot::SendMessage message(
                tgChatId,
                QTgBot::SendMessage::escapeMarkdownV2(
                    QStringLiteral("A user from %1 on XMPP would like to bridge this group. Use /continue or /abort."))
                .arg(u"**" % QTgBot::SendMessage::escapeMarkdownV2(mucJid) % u"**"),
                QTgBot::SendMessage::MarkdownV2);

    // try to send message to telegram
    await(d->tgClient->api()->sendMessage(message), this, [=](QTgBot::MessageResult &&result) {
        if (const auto error = std::get_if<QTgBot::Error>(&result)) {
            // bot not in telegram group or other issue
            const auto message = QStringLiteral("I'm sorry, I can't access this group. Have you added me already?");
            d->mucClient->sendBotMessage(mucJid, message);
            d->states.erase(std::remove(iter(d->states), mucJid), d->states.end());
        } else {
            // telegram seems to be okay, ask on XMPP whether this is the right telegram group
            await(d->tgClient->api()->getChat(tgChatId), this, [=](QTgBot::ChatResult &&result) {
                if (const auto chat = std::get_if<QTgBot::Chat>(&result)) {
                    const auto msg = QStringLiteral("Do you want to bridge this room with *%1* on Telegram? Use `/tg continue` or `/tg abort`.").arg(chat->title());
                    d->mucClient->sendBotMessage(mucJid, msg);

                    auto itr = std::find(iter(d->states), mucJid);
                    if (itr != d->states.end()) {
                        itr->xmppReadyToAccept = true;
                    }
                }
            });
        }
    });
}

bool BridgeSetup::continueSetup(const MucJid &mucJid)
{
    if (const auto itr = std::find(iter(d->groups), mucJid); itr != d->groups.end()) {
        if (d->bridgeCancellations.contains(mucJid)) {
            d->bridgeCancellations.remove(mucJid);
            d->database->removeGroup(mucJid);
            d->mucClient->removeBridge(mucJid);
            d->tgClient->removeBridge(itr->tgChatId);
            d->groups.erase(itr);
            onBridgeCancelled(mucJid);
            return true;
        }
    } else {
        if (const auto itr = std::find(d->states.begin(), d->states.end(), mucJid);
                itr != d->states.end()) {
            if (!itr->xmppReadyToAccept || itr->xmppAccepted) {
                return false;
            }
            itr->xmppAccepted = true;
            if (itr->accepted()) {
                onSetupAccepted(std::distance(d->states.begin(), itr));
            }
            return true;
        }
    }
    return false;
}

bool BridgeSetup::continueSetup(int64_t chatId)
{
    if (const auto itr = std::find(d->states.begin(), d->states.end(), chatId);
            itr != d->states.end()) {
        if (itr->tgAccepted) {
            return false;
        }
        itr->tgAccepted = true;
        if (itr->accepted()) {
            onSetupAccepted(std::distance(d->states.begin(), itr));
        }
        return true;
    }
    return false;
}

bool BridgeSetup::abortSetup(const MucJid &mucJid)
{
    if (const auto itr = std::find(iter(d->groups), mucJid); itr != d->groups.end()) {
        if (d->bridgeCancellations.contains(mucJid)) {
            d->bridgeCancellations.remove(mucJid);
            return true;
        }
    } else {
        if (const auto itr = std::find(d->states.cbegin(), d->states.cend(), mucJid);
                itr != d->states.cend()) {
            d->states.remove(std::distance(d->states.cbegin(), itr));
            return true;
        }
    }
    return false;
}

bool BridgeSetup::abortSetup(int64_t chatId)
{
    if (const auto itr = std::find(d->states.cbegin(), d->states.cend(), chatId);
            itr != d->states.cend()) {
        d->states.remove(std::distance(d->states.cbegin(), itr));
        return true;
    }
    return false;
}

bool BridgeSetup::cancelBridge(const MucJid &mucJid)
{
    if (std::find(iterc(d->groups), mucJid) != d->groups.cend()) {
        d->bridgeCancellations.insert(mucJid);
        return true;
    }
    return false;
}

void BridgeSetup::onSetupAccepted(qsizetype i)
{
    const auto state = d->states.at(i);
    const auto setupMessage = QStringLiteral("Bridge set up successfully!");
    d->mucClient->sendBotMessage(state.mucJid, setupMessage);
    d->tgClient->api()->sendMessage({state.tgChatId, setupMessage});

    const auto newGroup = DbGroup {
        DbGroup::TelegramMuc,
        state.mucJid,
        state.tgChatId,
    };

    d->groups << newGroup;
    d->database->insertGroup(newGroup);

    d->mucClient->addBridge({newGroup.mucJid});
    d->tgClient->addBridge(newGroup.tgChatId);

    sendToBridgeWatchers(
        QStringLiteral("A new bridge has been set up from '%1' on telegram to '%2' on XMPP/MUC.")
                .arg(QString::number(state.tgChatId), state.mucJid));
}

void BridgeSetup::onBridgeCancelled(const MucJid &mucJid)
{
    sendToBridgeWatchers(QStringLiteral("A bridge has been removed: ") + mucJid);
}

void BridgeSetup::sendToBridgeWatchers(const QString &text)
{
    QSettings settings;
    const auto watchers = settings.value(QStringLiteral("bridge/watchers")).toStringList();
    for (const auto &user : watchers) {
        if (!user.contains(u'@')) {
            continue;
        }
        d->mucClient->sendBotMessage(Jid { user }, text);
    }
}
