// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

#pragma once

#include <memory>
#include <variant>
#include <QObject>
#include <QVector>

class Database;
class DbGroup;
class TgBotClient;
class MucService;
struct MucJid;
struct BridgeSetupPrivate;

class BridgeSetup : public QObject
{
    Q_OBJECT
public:
    BridgeSetup(Database *database, QVector<DbGroup> &groups, QObject *parent = nullptr);
    ~BridgeSetup();
    void setMucClient(MucService *mucClient);
    void setTgClient(TgBotClient *tgClient);

    void addSetup(const MucJid &mucJid, int64_t tgChatId);
    bool continueSetup(const MucJid &mucJid);
    bool continueSetup(int64_t chatId);
    bool abortSetup(const MucJid &mucJid);
    bool abortSetup(int64_t chatId);

    bool cancelBridge(const MucJid &mucJid);

    void onSetupAccepted(qsizetype i);
    void onBridgeCancelled(const MucJid &mucJid);

    void sendToBridgeWatchers(const QString &text);

private:
    std::unique_ptr<BridgeSetupPrivate> d;
};
