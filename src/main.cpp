// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

#include <QCoreApplication>
#include "bridgecontroller.h"

constexpr QStringView APPLICATION_NAME = u"qxbridge";
constexpr QStringView APPLICATION_VERSION = u"0.1.0";
constexpr QStringView ORGANISATION_NAME = u"kaidan";
constexpr QStringView ORGANISATION_DOMAIN = u"kaidan.im";

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    app.setApplicationName(APPLICATION_NAME.toString());
    app.setApplicationVersion(APPLICATION_VERSION.toString());
    app.setOrganizationName(ORGANISATION_NAME.toString());
    app.setOrganizationDomain(ORGANISATION_DOMAIN.toString());

    BridgeController controller;

    return app.exec();
}
