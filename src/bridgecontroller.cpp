// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

#include "bridgecontroller.h"

#include <QFutureWatcher>

#include <QXmppMessage.h>

#include "qtgbot/api.h"
#include "qtgbot/sendmessage.h"
#include "qtgbot/message.h"

#include "tgbotclient.h"
#include "xmppservice.h"
#include "mucservice.h"
#include "bridgesetup.h"
#include "database.h"
#include "global.h"
#include "structs.h"

constexpr QStringView TELEGRAM_ICON_URL = u"https://telegram.org/img/t_logo.png";

class BridgeControllerPrivate
{
public:
    BridgeControllerPrivate(QObject *parent)
        : database(new Database),
          setup(new BridgeSetup(database.get(), groups, parent))
    {
    }

    std::unique_ptr<Database> database;
    BridgeSetup *setup;
    QVector<DbGroup> groups;
};

BridgeController::BridgeController(QObject *parent)
    : QObject(parent),
      d(new BridgeControllerPrivate(this)),
      m_tgClient(new TgBotClient(d->database.get(), d->setup, this)),
      m_xmppService(new XmppService(d->setup, this))
{
    connect(m_tgClient, &TgBotClient::messageReceived, this, &BridgeController::onTgMessage);
    connect(m_tgClient, &TgBotClient::memberAdded, this, &BridgeController::onTgMemberAdded);
    connect(m_tgClient, &TgBotClient::memberRemoved, this, &BridgeController::onTgMemberRemoved);
    connect(m_tgClient, &TgBotClient::memberPhotoChanged, this, &BridgeController::onTgMemberPhotoChanged);
    connect(m_xmppService, &XmppService::connected, this, &BridgeController::onXmppConnected);
    connect(m_xmppService->mucService(), &MucService::messageReceived, this, &BridgeController::onMucMessage);

    await(d->database->groups(), this, [this](QVector<DbGroup> &&groups) {
        d->groups = groups;
        m_tgClient->start();
        m_xmppService->connectToServer();
    });
    await(m_tgClient->api()->download(TELEGRAM_ICON_URL.toString()), this, [this](QTgBot::DownloadResult &&result) {
        if (std::holds_alternative<QByteArray>(result)) {
            m_xmppService->mucService()->setBotPhoto(std::move(std::get<QByteArray>(result)));
        } else {
            debug() << "Couldn't fetch telegram icon:" << std::get<QTgBot::Error>(result).description;
        }
    });
}

BridgeController::~BridgeController() = default;

void BridgeController::onXmppConnected()
{
    const auto tgMembers = m_tgClient->currentMembers();
    std::for_each(tgMembers.keyBegin(), tgMembers.keyEnd(), [&](qint64 tgChatId) {
        if (const auto mucJid = mapTgChatIdToMucJid(tgChatId); !mucJid.isEmpty()) {
            m_xmppService->mucService()->addBridge({mucJid});
        }
        for (const auto &user : tgMembers.value(tgChatId)) {
            onTgMemberAdded(tgChatId, user);
        }
    });
}

void BridgeController::onMucMessage(const MucJid &mucJid, const QString &user, const QString &text)
{
    if (const auto tgChatId = mapMucJidToTgChat(mucJid)) {
        qDebug() << mucJid << tgChatId;
        const auto escaped = QStringLiteral("*%1*: %2")
                .arg(QTgBot::SendMessage::escapeMarkdownV2(user), QTgBot::SendMessage::escapeMarkdownV2(text));

        auto future = m_tgClient->api()->sendMessage({tgChatId, escaped, QTgBot::SendMessage::MarkdownV2});
        await(future, this, [](const QTgBot::MessageResult &result) {
            if (const auto message = std::get_if<QTgBot::Message>(&result)) {
                qDebug() << "> Message sent to telegram.";
            } else if (const auto error = std::get_if<QTgBot::Error>(&result)) {
                qDebug() << "| Telegram: sendMessage failed:" << error->description;
            }
        });
    }
}

void BridgeController::onTgMessage(qint64 tgChatId, int uid, const ChatMessage &msg)
{
    if (!m_xmppService->isConnected()) {
        return;
    }

    if (const auto mucJid = mapTgChatIdToMucJid(tgChatId); !mucJid.isEmpty()) {
        m_xmppService->mucService()->sendMessage(mucJid, uid, msg);
    }
}

void BridgeController::onTgMemberAdded(qint64 tgChatId, const ChatUser &user)
{
    if (!m_xmppService->isConnected()) {
        return;
    }

    if (const auto mucJid = mapTgChatIdToMucJid(tgChatId); !mucJid.isEmpty()) {
        m_xmppService->mucService()->addMember(mucJid, user);
    }
}

void BridgeController::onTgMemberRemoved(qint64 tgChatId, int uid)
{
    if (!m_xmppService->isConnected()) {
        return;
    }

    if (const auto mucJid = mapTgChatIdToMucJid(tgChatId); !mucJid.isEmpty()) {
        m_xmppService->mucService()->removeMember(mucJid, uid);
    }
}

void BridgeController::onTgMemberPhotoChanged(qint64 tgChatId, int uid, const QByteArray &data)
{
    if (const auto mucJid = mapTgChatIdToMucJid(tgChatId); !mucJid.isEmpty()) {
        m_xmppService->mucService()->updateMemberPhoto(mucJid, uid, data);
    }
}

QString BridgeController::mapTgChatIdToMucJid(qint64 tgChatId)
{
    const auto itr = std::find_if(d->groups.cbegin(), d->groups.cend(), [&tgChatId](const DbGroup &group) {
        return group.tgChatId == tgChatId;
    });
    if (itr != d->groups.cend()) {
        return itr->mucJid;
    }
    return {};
}

int64_t BridgeController::mapMucJidToTgChat(const MucJid &mucJid)
{
    if (const auto itr = std::find(d->groups.cbegin(), d->groups.cend(), mucJid);
            itr != d->groups.cend()) {
        return itr->tgChatId;
    }
    return 0;
}

QDebug BridgeController::debug() const
{
    return qDebug() << "[BridgeController]";
}
