// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

#include "tgbotclient.h"

#include <QTimer>
#include <QSettings>
#include <QFutureWatcher>
#include <QFile>
#include <QDir>
#include <QRandomGenerator>
#include <QStringBuilder>
#include <QMimeDatabase>
#include "qtgbot/api.h"
#include "qtgbot/chatmember.h"
#include "qtgbot/update.h"
#include "qtgbot/message.h"
#include "qtgbot/chatmemberupdated.h"
#include "qtgbot/photosize.h"
#include "qtgbot/file.h"
#include "qtgbot/document.h"
#include "database.h"
#include "structs.h"
#include "global.h"
#include "bridgesetup.h"

using namespace QTgBot;

struct TgUser : public DbTgUser
{
    int uid;
};

QString userNameFromUser(const QTgBot::User &user)
{
    if (!user.username().isEmpty()) {
        return user.username();
    }

#if QT_VERSION >= QT_VERSION_CHECK(5, 14, 0)
    static constexpr QStringView allowedChars = u"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-";
#else
    const auto allowedChars = QStringLiteral("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-");
#endif
    const auto fullName = user.firstName() + user.lastName();

    QString out;
    QTextStream stream(&out);
    for (const auto &character : fullName) {
        if (allowedChars.contains(character)) {
            stream << character;
        }
    }
    stream.flush();

    if (!out.isEmpty()) {
        return out;
    }

    return u"TgUser" % QString::number(user.id());
}

class TgBotClientPrivate
{
public:
    TgBotClient *q = nullptr;
    QTgBot::Api *api = nullptr;
    Database *database = nullptr;
    BridgeSetup *setup = nullptr;
    QTimer *updateTimer = nullptr;
    bool initializedCache = false;
    uint initializingTasks = 0;
    bool active = false;
    int updatesOffset = 0;

    // chat_id -> user_id -> tg user
    QHash<qint64, QHash<qint64, TgUser>> members;

    template<typename T, typename Handler>
    void initializationTask(const QFuture<T> future, Handler handler)
    {
        initializingTasks++;
        auto *watcher = new QFutureWatcher<T>(q);
        QObject::connect(watcher, &QFutureWatcherBase::finished, q, [watcher, handler { std::move(handler) }, this]() {
            handler(watcher->result());

            // if all initialization tasks are done now, start with the actual connection
            initializingTasks--;
            if (!initializingTasks) {
                q->onInitialized();
            }

            watcher->deleteLater();
        });
        watcher->setFuture(future);
    }
};

TgBotClient::TgBotClient(Database *database, BridgeSetup *setup, QObject *parent)
    : QObject(parent),
      d(new TgBotClientPrivate())
{
    QSettings settings;

    d->q = this;
    d->setup = setup;

    d->api = new Api(new QNetworkAccessManager(this), this);
    d->api->setApiToken(settings.value("tg/token").toString());

    d->database = database;

    d->updateTimer = new QTimer(this);
    d->updateTimer->setInterval(500);
    d->updateTimer->setSingleShot(true);
    connect(d->updateTimer, &QTimer::timeout, this, &TgBotClient::onUpdateTimer);

    d->setup->setTgClient(this);
}

TgBotClient::~TgBotClient()
{
    d->updateTimer->stop();
}

void TgBotClient::start()
{
    if (!d->initializedCache) {
        // check whether already initializing
        if (d->initializingTasks == 0) {
            initialize();
        }
    } else if (!d->active) {
        d->active = true;
        onUpdateTimer();
    }
}

void TgBotClient::stop()
{
    if (d->active) {
        d->active = false;
        if (d->updateTimer->isActive()) {
            d->updateTimer->stop();
        }
    }
}

Api *TgBotClient::api() const
{
    return d->api;
}

QHash<qint64, QVector<ChatUser>> TgBotClient::currentMembers()
{
    QHash<qint64, QVector<ChatUser>> members;
    for (const auto &chatUsers : std::as_const(d->members)) {
        for (const auto &user : chatUsers) {
            members[user.tgChatId] << ChatUser {
                user.uid,
                QString::number(user.tgUserId),
                user.username
            };
        }
    }
    return members;
}

void TgBotClient::addBridge(int64_t tgChatId)
{
    if (d->members.contains(tgChatId)) {
        const auto &chatUsers = d->members[tgChatId];
        for (const auto &user : chatUsers) {
            emit memberAdded(tgChatId, ChatUser {
                user.uid,
                QString::number(user.tgUserId),
                user.username
            });
        }
    }
}

void TgBotClient::removeBridge(int64_t tgChatId)
{
    await(d->api->sendMessage({tgChatId, QStringLiteral("The bridge has been destructed! 💥💥 C ya!")}), this, [=](auto) {
        // make sure the chat is left after the message has been sent
        d->api->leaveChat(tgChatId);
    });
}

void TgBotClient::onUpdateTimer()
{
    await(d->api->getUpdates(d->updatesOffset), this, [this](const UpdatesResult &result) {
        if (const auto updates = std::get_if<Updates>(&result)) {
            for (const auto &update : *updates) {
                handleUpdate(update);
            }
            if (!updates->empty()) {
                d->updatesOffset = updates->back().updateId() + 1;
            }
        } else {
            qDebug() << "[TgBotClient] Error fetching updates:" << std::get<Error>(result).description;
        }

        // check whether client is still active
        if (d->active) {
            d->updateTimer->start();
        }
    });
}

void TgBotClient::onMessageAttachment(const Message &message, const QString &fileId, const QString &fileName, const QString &mimeTypeName)
{
    await(d->api->getFile(fileId), this, [=](FileResult &&result) {
        if (auto file = std::get_if<File>(&result)) {
            const auto [subdir, relativeFilePath] = [=]() -> std::pair<QString, QString> {
                if (!fileName.isEmpty()) {
                    const auto subdir = QString::number(QRandomGenerator::system()->generate64(), 36);
                    return { subdir, subdir % u'/' % fileName };
                }
                QMimeDatabase mimeDb;
                auto mimeType = mimeDb.mimeTypeForName(mimeTypeName);
                return { "0", u"0/" % fileId % u'.' % mimeType.preferredSuffix() };
            }();

            QSettings settings;
            const QDir outputDir(settings.value("xmpp/http-file-upload-dir").toString());
            if (!subdir.isEmpty()) {
                if (!outputDir.exists(subdir)) {
                    outputDir.mkpath(subdir);
                }
            }

            auto localFile = std::make_unique<QFile>(outputDir.absoluteFilePath(relativeFilePath));
            await(d->api->downloadFilePathToFile(file->filePath(), std::move(localFile)),
                  this, [=, relativeFilePath { std::move(relativeFilePath) }](DownloadFileResult &&result) {
                if (std::holds_alternative<True>(result)) {
                    QSettings settings;
                    ChatMessage msg;
                    msg.text = message.text().isEmpty() ? message.caption() : message.text();
                    msg.mimeType = mimeTypeName;
                    msg.url = settings.value("xmpp/http-file-upload-url").toString() % relativeFilePath;

                    const auto chatId = message.chat().id();
                    const auto userId = message.from()->id();

                    emit messageReceived(chatId, memberUid(chatId, userId), msg);
                }
            });
        } else {
            qDebug() << "TgBotClient::onMessageAttachment" << std::get<Error>(result).description;
        }
    });
}

void TgBotClient::handleUpdate(const Update &update)
{
    if (const auto message = update.message()) {
//        qDebug() << "Message received:" << message->chat().title() << message->from()->firstName() << message->text();
        const auto chatId = message->chat().id();
        const auto userId = message->from()->id();

        if (message->from() && !hasMember(message->chat().id(), message->from()->id())) {
            addMember({
                chatId,
                userId,
                userNameFromUser(*message->from()),
            });
        }

        if (const auto sizes = message->photo(); !sizes.empty()) {
            onMessageAttachment(*message, sizes.last().fileId(), {}, QStringLiteral("image/jpeg"));
        } else if (auto doc = message->animation()) {
            onMessageAttachment(*message, doc->fileId(), {}, doc->mimeType());
        } else if (auto doc = message->audio()) {
            onMessageAttachment(*message, doc->fileId(), {}, doc->mimeType());
        } else if (auto doc = message->document()) {
            onMessageAttachment(*message, doc->fileId(), doc->fileName(), doc->mimeType());
        } else if (auto doc = message->sticker()) {
            onMessageAttachment(*message, doc->fileId(), {}, doc->mimeType());
        } else if (auto doc = message->video()) {
            onMessageAttachment(*message, doc->fileId(), {}, doc->mimeType());
        } else if (auto doc = message->videoNote()) {
            onMessageAttachment(*message, doc->fileId(), {}, doc->mimeType());
        } else if (auto doc = message->voice()) {
            onMessageAttachment(*message, doc->fileId(), {}, doc->mimeType());
        } else if (!handleChatCommands(*message)) {
            ChatMessage msg;
            msg.text = message->text();

            emit messageReceived(chatId, memberUid(chatId, userId), msg);
        }

    }
    if (const auto memberUpdate = update.chatMember()) {
        const auto chatId = memberUpdate->chat().id();
        const auto userId = memberUpdate->from().id();
        if (!hasMember(chatId, userId)) {
            addMember({
                chatId,
                userId,
                userNameFromUser(memberUpdate->from()),
            });
        } else if (const auto newName = userNameFromUser(memberUpdate->newChatMember().user());
                   newName != userNameFromUser(memberUpdate->oldChatMember().user())) {
            auto &member = d->members[chatId][userId];

            emit memberRemoved(member.tgChatId, member.uid);
            member.username = newName;
            // update/replace in database
            d->database->insertTgUser(member);
            emit memberAdded(member.tgChatId,
                             { member.uid, QString::number(member.tgUserId), member.username });
        }
    }
}

bool TgBotClient::handleChatCommands(const Message &message)
{
    const auto send = [=](const QString &text) {
        d->api->sendMessage({
            message.chat().id(),
            text,
            SendMessage::None,
            message.messageId()
        });
    };

    const auto text = message.text();
    if (text.isEmpty()) {
        return false;
    }

    if (text.startsWith(u"/id")) {
        send(QString::number(message.chat().id()));
    } else if (text.startsWith(u"/abort")) {
        if (d->setup->abortSetup(message.chat().id())) {
            send(QStringLiteral("Aborted the setup."));
        } else {
            send(QStringLiteral("There's no setup to abort."));
        }
    } else if (text.startsWith(u"/continue")) {
        if (d->setup->continueSetup(message.chat().id())) {
            send(QStringLiteral("Accepted."));
        } else {
            send(QStringLiteral("There's no setup to continue."));
        }
    } else {
        return false;
    }
    return true;
}

void TgBotClient::initialize()
{
    d->initializationTask(d->database->groups(), [this](const QVector<DbGroup> &groups) {
        for (const auto &group : groups) {
            if (group.tgChatId != 0) {
                initializeMembers(group.tgChatId);
            }
        }
    });
}

void TgBotClient::initializeMembers(qint64 chatId)
{
    d->initializationTask(d->database->tgUsers(chatId),
                          [this](const QVector<DbTgUser> &members) {
        for (const auto &member : members) {
            d->initializationTask(d->api->getChatMember(member.tgChatId, member.tgUserId),
                                  [this, member](const ChatMemberResult &result) {
                if (std::holds_alternative<Error>(result)) {
                    // user isn't in this chat anymore
                    d->database->removeTgUser(member.tgChatId, member.tgUserId);
                } else if (const auto chatMember = std::get_if<ChatMember>(&result)) {
                    // check if the user changed and update it in the database
                    if (member.username != chatMember->user().username()) {
                        DbTgUser newUser {
                            member.tgChatId,
                            member.tgUserId,
                            userNameFromUser(chatMember->user())
                        };

                        d->database->insertTgUser(newUser);
                        addMember(newUser);
                    } else {
                        addMember(member);
                    }
                }
            });
        }
    });
}

void TgBotClient::onInitialized()
{
    d->initializedCache = true;
    start();
}

void TgBotClient::addMember(const DbTgUser &member)
{
    if (!d->members.contains(member.tgChatId)) {
        d->members.insert(member.tgChatId, {});
    }

    TgUser user;
    user.uid = ChatUser::genUserId();
    user.tgChatId = member.tgChatId;
    user.tgUserId = member.tgUserId;
    user.username = member.username;

    d->members[member.tgChatId].insert(member.tgUserId, user);
    d->database->insertTgUser(member);
    emit memberAdded(user.tgChatId, { user.uid, QString::number(user.tgUserId), user.username });

    await(d->api->getUserProfilePhotos(user.tgUserId, 0, 0), this, [=](UserProfilePhotosResult &&result) {
        if (const auto photos = std::get_if<UserProfilePhotos>(&result)) {
            if (photos->photos().isEmpty()) {
                return;
            }
            const auto photoSize = photos->pick(320);
            await(d->api->getFile(photoSize.fileId()), this, [=](FileResult &&result) {
                if (const auto file = std::get_if<File>(&result)) {
                    qDebug() << "File info received" << file->filePath() << QLocale::system().formattedDataSize(file->fileSize());
                    if (file->fileSize() >= 50 * 1024 * 1024) {
                        qDebug() << "File too large.";
                        return;
                    }
                    await(d->api->downloadFilePath(file->filePath()), this, [=](DownloadResult &&result) {
                        if (const auto data = std::get_if<QByteArray>(&result)) {
                            emit memberPhotoChanged(user.tgChatId, user.uid, *data);
                        } else {
                            qDebug() << std::get<Error>(result).description;
                        }
                    });
                } else {
                    qDebug() << std::get<Error>(result).description;
                }
            });
        }
    });
}

bool TgBotClient::hasMember(qint64 chatId, qint64 userId)
{
    if (d->members.contains(chatId)) {
        return d->members[chatId].contains(userId);
    }
    return false;
}

int TgBotClient::memberUid(qint64 chatId, qint64 userId)
{
    auto &chatUsers = d->members[chatId];
    if (chatUsers.contains(userId)) {
        return chatUsers[userId].uid;
    }
    return 0;
}
