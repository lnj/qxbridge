// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

#pragma once

#include <memory>
#include <QObject>

namespace QTgBot {
class Message;
}

class Database;
class TgBotClient;
class XmppService;
class QXmppMessage;
class ChatUser;
class ChatMessage;
class MucJid;
class BridgeControllerPrivate;

class BridgeController : public QObject
{
    Q_OBJECT

public:
    explicit BridgeController(QObject *parent = nullptr);
    ~BridgeController();

private:
    Q_SLOT void onXmppConnected();
    Q_SLOT void onMucMessage(const MucJid &mucJid, const QString &user, const QString &text);
    // telegram
    Q_SLOT void onTgMessage(qint64 tgChatId, int uid, const ChatMessage &text);
    Q_SLOT void onTgMemberAdded(qint64 tgChatId, const ChatUser &user);
    Q_SLOT void onTgMemberRemoved(qint64 tgChatId, int uid);
    Q_SLOT void onTgMemberPhotoChanged(qint64 tgChatId, int uid, const QByteArray &data);

    QString mapTgChatIdToMucJid(qint64 tgChatId);
    int64_t mapMucJidToTgChat(const MucJid &mucJid);
    QDebug debug() const;

    std::unique_ptr<BridgeControllerPrivate> d;
    TgBotClient *m_tgClient;
    XmppService *m_xmppService;
};
