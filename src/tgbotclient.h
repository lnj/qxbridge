// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

#pragma once

#include <memory>
#include <QObject>

class QTimer;

namespace QTgBot {
class Api;
class Update;
class Message;
class Document;
}

class TgBotClientPrivate;
class BridgeSetup;
class DbTgUser;
class ChatUser;
class ChatMessage;
class Database;

class TgBotClient : public QObject
{
    Q_OBJECT

public:
    TgBotClient(Database *database, BridgeSetup *setup, QObject *parent = nullptr);
    ~TgBotClient();

    void start();
    void stop();

    QTgBot::Api *api() const;

    QHash<qint64, QVector<ChatUser>> currentMembers();
    void addBridge(int64_t tgChatId);
    void removeBridge(int64_t tgChatId);

    Q_SIGNAL void messageReceived(qint64 tgChatId, int uid, const ChatMessage &text);
    Q_SIGNAL void memberAdded(qint64 tgChatId, const ChatUser &user);
    Q_SIGNAL void memberRemoved(qint64 tgChatId, int uid);
    Q_SIGNAL void memberPhotoChanged(qint64 tgChatId, int uid, const QByteArray &data);

private:
    Q_SLOT void onUpdateTimer();
    void onMessageAttachment(const QTgBot::Message &message, const QString &fileId, const QString &fileName, const QString &mimeTypeName);
    void handleUpdate(const QTgBot::Update &update);
    bool handleChatCommands(const QTgBot::Message &message);
    void initialize();
    void initializeMembers(qint64 chatId);
    void onInitialized();
    void addMember(const DbTgUser &member);
    bool hasMember(qint64 chatId, qint64 userId);
    int memberUid(qint64 chatId, qint64 userId);

    friend class TgBotClientPrivate;

    std::unique_ptr<TgBotClientPrivate> d;
};
