// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

#pragma once

#include <memory>
#include <variant>
#include <QDateTime>
#include <QXmppComponentExtension.h>

#include "structs.h"

class QXmppPresence;
class QXmppMessage;

class MucServicePrivate;
class BridgeSetup;
class ChatUser;
class ChatMessage;
struct MucUser;

class MucService : public QXmppComponentExtension
{
    Q_OBJECT
public:
    MucService(BridgeSetup *setup);
    ~MucService();

    bool handleStanza(const QDomElement &stanza) override;

    void addBridge(const MucJid &jid);
    void removeBridge(const MucJid &jid);
    void addMember(const QString &mucJid, const ChatUser &user);
    void removeMember(const QString &mucJid, int uid);
    void sendMessage(const QString &mucJid, int uid, const ChatMessage &msg);
    void updateMemberPhoto(const QString &mucJid, int uid, const QByteArray &data);
    void setBotPhoto(QByteArray &&data);
    void sendBotMessage(const std::variant<Jid, MucJid> &to, const QString &body);
    Q_SIGNAL void messageReceived(const MucJid &mucJid, const QString &user, const QString &text);

protected:
    void setComponent(QXmppComponent *component) override;

private:
    Q_SLOT void onConnected();
    Q_SLOT void onMessageReceived(const QXmppMessage &message);
    Q_SLOT void onPresenceReceived(const QXmppPresence &presence);
    bool handleChatCommands(const QXmppMessage &message);

    void connectMucUser(MucUser &user);
    void disconnectMucUser(MucUser &user);
    void resendPresence(const MucUser &user);
    void botJoinMuc(const QString &mucJid);
    void botLeaveMuc(const QString &mucJid);

    MucUser *findUser(const QString &jid);

    QString localUserFullJid(QStringView user);
    QString usernameToMucNick(const QString &username);
    const QString &botJid();
    const QString &botFullJid();

    std::unique_ptr<MucServicePrivate> d;
};
