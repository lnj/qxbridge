// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

#include "mucservice.h"

#include "QXmppComponent.h"
#include "QXmppComponentConfig.h"
#include <QXmppUtils.h>
#include <QXmppPresence.h>
#include <QXmppMessage.h>
#include <QXmppVCardIq.h>
#include <QXmppDiscoveryIq.h>

#include <QCryptographicHash>
#include <QFutureWatcher>
#include <QRandomGenerator>
#include <QDebug>
#include <QDomElement>
#include <QMimeDatabase>
#include <QMimeType>
#include <QStringBuilder>
#include <QSettings>
#include <QtConcurrentRun>

#include "structs.h"
#include "global.h"
#include "bridgesetup.h"

constexpr QStringView BOT_USER = u"bot";
constexpr QStringView BOT_MUC_NICK = u"Telegram";
constexpr QStringView USER_RESOURCE = u"tg";
constexpr QStringView USER_POSTFIX = u"[tg]";

struct AvatarData
{
    QByteArray data;
    QByteArray hash;
    QMimeType mime;

    static AvatarData fromData(QByteArray data)
    {
        return {
            data,
            QCryptographicHash::hash(data, QCryptographicHash::Sha1),
            QMimeDatabase().mimeTypeForData(data),
        };
    }
};

struct MucUser
{
    enum State {
        Disconnected,
        Connecting,
        Connected,
    };

    int uid;
    // local user JID on the component
    QString userJid;
    // room@muc.service/usernick
    QString mucFullJid;
    State state = Disconnected;
    AvatarData avatar;

    QXmppPresence presence() const
    {
        QXmppPresence presence;
        presence.setType(state == Disconnected ? QXmppPresence::Unavailable : QXmppPresence::Available);
        presence.setFrom(userJid);
        presence.setTo(mucFullJid);
        presence.setMucSupported(true);
        if (!avatar.hash.isEmpty()) {
            presence.setVCardUpdateType(QXmppPresence::VCardUpdateValidPhoto);
            presence.setPhotoHash(avatar.hash);
        } else {
            presence.setVCardUpdateType(QXmppPresence::VCardUpdateNotReady);
        }
        return presence;
    }
};

struct BotMucState
{
    bool joined = false;
    bool initial = false;
    QSet<QString> admins;
    Jid inviteeJid;
};

QString fullJid(QStringView user, QStringView domain, QStringView resource)
{
    return user % u'@' % domain % u'/' % resource;
}

class MucServicePrivate
{
public:
    QDateTime startTime = QDateTime::currentDateTimeUtc();
    AvatarData botAvatar;
    QHash<QString, BotMucState> botJoinedMucs;
    // MUC room -> (uid -> MucUser)
    QHash<QString, QHash<int, MucUser>> users;
    BridgeSetup *setup = nullptr;
};

MucService::MucService(BridgeSetup *setup)
    : d(new MucServicePrivate)
{
    d->setup = setup;
    d->setup->setMucClient(this);
}

MucService::~MucService()
{
}

bool MucService::handleStanza(const QDomElement &stanza)
{
    const auto sendVCard = [=](const QXmppVCardIq &request, const AvatarData &avatar) {
        QXmppVCardIq iq;
        iq.setId(request.id());
        iq.setFrom(request.to());
        iq.setTo(request.from());
        iq.setType(QXmppIq::Result);
        if (!avatar.data.isEmpty()) {
            iq.setPhoto(avatar.data);
            iq.setPhotoType(avatar.mime.name());
        }
        component()->sendPacket(iq);
    };

    if (stanza.tagName() == u"iq" && stanza.attribute("type") == u"get") {
        if (QXmppDiscoveryIq::isDiscoveryIq(stanza)) {
            QXmppDiscoveryIq request;
            request.parse(stanza);

            const auto to = QXmppUtils::jidToBareJid(request.to());
            if (to == botJid() || findUser(request.to())) {
                QXmppDiscoveryIq iq;
                iq.setId(request.id());
                iq.setType(QXmppIq::Result);
                iq.setFrom(request.to());
                iq.setTo(request.from());
                iq.setFeatures({
                    "vcard-temp",
                });
                component()->sendPacket(iq);
                return true;
            }
            QXmppDiscoveryIq iq;
            iq.setId(request.id());
            iq.setFrom(request.to());
            iq.setTo(request.from());
            iq.setType(QXmppIq::Error);
            iq.setError(QXmppStanza::Error(
                            QXmppStanza::Error::Type::Cancel,
                            QXmppStanza::Error::Condition::ItemNotFound,
                            "Couldn't find such a user."));
            component()->sendPacket(iq);
            return true;
        }

        if (QXmppVCardIq::isVCard(stanza)) {
            QXmppVCardIq request;
            request.parse(stanza);

            const auto to = QXmppUtils::jidToBareJid(request.to());
            if (to == botJid()) {
                sendVCard(request, d->botAvatar);
                return true;
            } else if (auto *user = findUser(request.to())) {
                sendVCard(request, user->avatar);
                return true;
            }

            QXmppVCardIq iq;
            iq.setId(request.id());
            iq.setFrom(request.to());
            iq.setTo(request.from());
            iq.setType(QXmppIq::Error);
            iq.setError(QXmppStanza::Error(
                            QXmppStanza::Error::Type::Cancel,
                            QXmppStanza::Error::Condition::ItemNotFound,
                            "Couldn't find such a user."));
            component()->sendPacket(iq);
            qDebug() << "Sent VCARD error: Not found";
            return true;
        }
    }
    return false;
}

void MucService::addBridge(const MucJid &mucJid)
{
    if (!d->botJoinedMucs.contains(mucJid)) {
        d->botJoinedMucs[mucJid];
        botJoinMuc(mucJid);
    }
    d->users[mucJid];
}

void MucService::removeBridge(const MucJid &jid)
{
    if (d->botJoinedMucs.contains(jid)) {
        sendBotMessage(jid, QStringLiteral("The bridge has been destructed, C ya! 🌉💣"));
        for (auto &user : d->users[jid]) {
            disconnectMucUser(user);
        }
        botLeaveMuc(jid);

        d->users.remove(jid);
        d->botJoinedMucs.remove(jid);
    }
}

void MucService::addMember(const QString &mucJid, const ChatUser &user)
{
    if (!d->botJoinedMucs.contains(mucJid)) {
        d->botJoinedMucs[mucJid];
        botJoinMuc(mucJid);
    }

    auto &mucUsers = d->users[mucJid];
    const auto mucNick = usernameToMucNick(user.nickname);
    qDebug() << "[MucServer] Join:" << mucJid << mucNick;
    mucUsers.insert(user.uid, {
        user.uid,
        fullJid(user.protocolId, component()->configuration().componentName(), USER_RESOURCE),
        mucJid % u'/' % mucNick,
        MucUser::Disconnected,
        AvatarData()
    });
    connectMucUser(mucUsers[user.uid]);
}

void MucService::removeMember(const QString &mucJid, int uid)
{
    auto &mucUsers = d->users[mucJid];
    if (mucUsers.contains(uid)) {
        disconnectMucUser(mucUsers[uid]);
        mucUsers.remove(uid);
    }
}

void MucService::sendMessage(const QString &mucJid, int uid, const ChatMessage &message)
{
    auto &mucUsers = d->users[mucJid];
    if (!mucUsers.contains(uid)) {
        return;
    }

    // mentions
    auto body = message.text;
    for (auto &user : mucUsers) {
        auto tgUsername = QXmppUtils::jidToResource(user.mucFullJid);
        tgUsername.chop(USER_POSTFIX.size());
        body.replace(u'@' + tgUsername, tgUsername % USER_POSTFIX);
    }

    if (!message.url.isEmpty()) {
        QXmppMessage mediaMessage;
        mediaMessage.setType(QXmppMessage::GroupChat);
        mediaMessage.setFrom(mucUsers[uid].userJid);
        mediaMessage.setTo(mucJid);
        mediaMessage.setBody(message.url);
        mediaMessage.setOutOfBandUrl(message.url);

        component()->sendPacket(mediaMessage);
    }

    if (!message.text.isEmpty()) {
        QXmppMessage textMessage;
        textMessage.setType(QXmppMessage::GroupChat);
        textMessage.setFrom(mucUsers[uid].userJid);
        textMessage.setTo(mucJid);
        textMessage.setBody(body);
        component()->sendPacket(textMessage);
    }
}

void MucService::updateMemberPhoto(const QString &mucJid, int uid, const QByteArray &data)
{
    await(QtConcurrent::run(AvatarData::fromData, data), this, [=](AvatarData &&avatar) {
        auto &user = d->users[mucJid][uid];
        user.avatar = avatar;
        if (user.state != MucUser::Disconnected) {
            resendPresence(user);
        }
    });
}

void MucService::setBotPhoto(QByteArray &&data)
{
    await(QtConcurrent::run(AvatarData::fromData, std::move(data)), this, [this](AvatarData &&avatar) {
        d->botAvatar = avatar;

        // refresh presence with new avatar
        std::for_each(d->botJoinedMucs.keyBegin(), d->botJoinedMucs.keyEnd(), [=](const QString &mucJid) {
            botJoinMuc(mucJid);
        });
    });
}

void MucService::setComponent(QXmppComponent *component)
{
    QXmppComponentExtension::setComponent(component);
    connect(component, &QXmppComponent::presenceReceived, this, &MucService::onPresenceReceived);
    connect(component, &QXmppComponent::messageReceived, this, &MucService::onMessageReceived);
    connect(component, &QXmppComponent::connected, this, &MucService::onConnected);
}

void MucService::onConnected()
{
}

void MucService::onMessageReceived(const QXmppMessage &message)
{
    // ignore MUC history
    if (message.stamp().isValid()) { //&& message.stamp().toUTC() <= d->startTime) {
        return;
    }

    const auto to = QXmppUtils::jidToBareJid(message.to());
    if (to == botJid()) {
        // check for invitations
        if (const auto mucJid = message.mucInvitationJid(); !mucJid.isEmpty()) {
            if (d->botJoinedMucs.contains(mucJid)) {
                return;
            }

            d->botJoinedMucs[mucJid].initial = true;
            botJoinMuc(mucJid);
        } else if (handleChatCommands(message)) {
        } else {
            const auto mucJid = MucJid { QXmppUtils::jidToBareJid(message.from()) };
            const QString botMucJid = QString(mucJid) % u'/' % BOT_MUC_NICK;
            if (message.body().isEmpty() || message.from() == botMucJid || !d->users.contains(mucJid)) {
                return;
            }

            // ignore messages from bridged users (our own messages)
            auto &bridgedUsers = d->users[mucJid];
            for (auto &user : bridgedUsers) {
                if (user.mucFullJid == message.from()) {
                    return;
                }
            }

            // process message
            // mentions
            auto body = message.body();
            for (auto &user : bridgedUsers) {
                auto mucNick = QXmppUtils::jidToResource(user.mucFullJid);
                auto tgUsername = u'@' + mucNick;
                tgUsername.chop(USER_POSTFIX.size());
                body = body.replace(mucNick, tgUsername);
            }

            emit messageReceived(mucJid, QXmppUtils::jidToResource(message.from()), body);
        }
        return;
    }
}

void MucService::onPresenceReceived(const QXmppPresence &presence)
{
    if (presence.to() == botFullJid()) {
        if (d->botJoinedMucs.contains(presence.from())) {
            // presence from MUC itself
            // bot joining
            const auto mucJid = presence.from();
            auto &state = d->botJoinedMucs[mucJid];
            if (presence.type() == QXmppPresence::Error) {
                if (!state.inviteeJid.isEmpty()) {
                    sendBotMessage(state.inviteeJid, QStringLiteral("Sorry I couldn't join that MUC: ") + presence.error().text());
                }
                d->botJoinedMucs.remove(mucJid);
            } else {
                state.joined = true;
                if (state.initial) {
                    const auto mucJid = MucJid { presence.from() };
                    sendBotMessage(
                        mucJid,
                        QStringLiteral("Hi, I'm a Telegram/XMPP bridge. Use `/tg help` for help.")
                    );
                    state.initial = false;
                }
            }
        } else if (const auto bareJid = QXmppUtils::jidToBareJid(presence.from());
                   d->botJoinedMucs.contains(bareJid)) {
            // presences from MUC users
            auto &mucState = d->botJoinedMucs[bareJid];

            // Affiliation changes
            const auto mucItem = presence.mucItem();
            if (!mucItem.isNull()) {
                switch (mucItem.affiliation()) {
                case QXmppMucItem::Affiliation::AdminAffiliation:
                case QXmppMucItem::Affiliation::OwnerAffiliation:
                    mucState.admins.insert(presence.from());
                    break;
                case QXmppMucItem::Affiliation::NoAffiliation:
                case QXmppMucItem::Affiliation::UnspecifiedAffiliation:
                case QXmppMucItem::Affiliation::MemberAffiliation:
                case QXmppMucItem::Affiliation::OutcastAffiliation:
                    mucState.admins.remove(presence.from());
                    break;
                }
            }
        }

        return;
    }

    // MUC Joining:
    // check for presences from the MUC itself (no resource)
    if (d->users.contains(presence.from())) {
        auto &mucUsers = d->users[presence.from()];

        for (auto &mucUser : mucUsers) {
            if (mucUser.userJid == presence.to()) {
                if (presence.type() == QXmppPresence::Type::Error) {
                    mucUser.state = MucUser::Disconnected;
                    // todo handle this (check error: nickname conflict ?)
                } else {
                    mucUser.state = MucUser::Connected;
                }
                break;
            }
        }
    }
}

bool MucService::handleChatCommands(const QXmppMessage &message)
{
    static constexpr QStringView COMMAND_PREFIX = u"/tg ";
    if (!message.body().startsWith(COMMAND_PREFIX)) {
        return false;
    }

    const auto mucJid = MucJid { QXmppUtils::jidToBareJid(message.from()) };
    const auto isAuthorized = d->botJoinedMucs[mucJid].admins.contains(message.from());
    const auto command = message.body().mid(COMMAND_PREFIX.size());
    const auto send = [=](const auto &text) {
        sendBotMessage(mucJid, text);
    };
    const auto sendNotAuthored = [=]() {
        send(QStringLiteral("You need to be an admin or owner in this MUC to complete this action."));
        return true;
    };

    if (command.startsWith(u"help")) {
        send(QStringLiteral(R"(All commands need to be prefixed with `/tg `.

* `help`: Show this help.
* `bridge <telegram chat id>`: Setups a bridge between this channel and Telegram.
* `cancel`: Cancels the bridge between this channel and Telegram.

Creating bridges works like this:
* Add me on Telegram and give me access to messages.
* Send `/id` on Telegram to get the chat ID.
* Invite me to your XMPP channel and send `/tg bridge <tg chat id>`)"));
    } else if (command.startsWith(u"bridge")) {
        if (!isAuthorized) {
            return sendNotAuthored();
        }
        const auto args = command.split(u' ');
        if (args.size() != 2) {
            send(QStringLiteral(u"Requires `<id>` as an argument."));
            return true;
        }
        bool ok;
        if (const auto tgChatId = args[1].toLongLong(&ok); ok) {
            d->setup->addSetup(mucJid, tgChatId);
        } else {
            send(QStringLiteral(u"Are you sure that is a correct Telegram Chat ID? Get the value by using `/id` in the Telegram group."));
        }
    } else if (command.startsWith(u"abort")) {
        if (!isAuthorized) {
            return sendNotAuthored();
        }
        if (d->setup->abortSetup(mucJid)) {
            send(QStringLiteral("Aborted."));
        } else {
            send(QStringLiteral("There's no setup to abort."));
        }
    } else if (command.startsWith(u"continue")) {
        if (!isAuthorized) {
            return sendNotAuthored();
        }
        if (d->setup->continueSetup(mucJid)) {
            send(QStringLiteral("Accepted."));
        } else {
            send(QStringLiteral("There's no setup to continue."));
        }
    } else if (command.startsWith(u"cancel")) {
        if (!isAuthorized) {
            return sendNotAuthored();
        }
        if (d->setup->cancelBridge(mucJid)) {
            send(QStringLiteral("Do you really want to remove the bridge between this MUC and the Telegram group? `/tg continue` or `/tg abort`"));
        } else {
            send(QStringLiteral("There's no bridge set up."));
        }
    } else {
        return false;
    }
    return true;
}

void MucService::connectMucUser(MucUser &user)
{
    user.state = MucUser::Connecting;
    component()->sendPacket(user.presence());
}

void MucService::disconnectMucUser(MucUser &user)
{
    user.state = MucUser::Disconnected;
    component()->sendPacket(user.presence());
}

void MucService::resendPresence(const MucUser &user)
{
    component()->sendPacket(user.presence());
}

void MucService::sendBotMessage(const std::variant<Jid, MucJid> &to, const QString &body)
{
    QXmppMessage msg;
    msg.setId(QString::number(QRandomGenerator::system()->generate64(), 36));
    msg.setFrom(botFullJid());
    if (const auto jid = std::get_if<Jid>(&to)) {
        msg.setType(QXmppMessage::Chat);
        msg.setTo(QXmppUtils::jidToBareJid(std::get<Jid>(to)));
    } else {
        msg.setType(QXmppMessage::GroupChat);
        msg.setTo(QXmppUtils::jidToBareJid(std::get<MucJid>(to)));
    }
    msg.setBody(body);
    component()->sendPacket(msg);
}

void MucService::botJoinMuc(const QString &mucJid)
{
    QXmppPresence presence;
    presence.setType(QXmppPresence::Available);
    presence.setFrom(botFullJid());
    presence.setTo(mucJid % u'/' % BOT_MUC_NICK);
    presence.setMucSupported(true);
    if (!d->botAvatar.hash.isEmpty()) {
        presence.setVCardUpdateType(QXmppPresence::VCardUpdateValidPhoto);
        presence.setPhotoHash(d->botAvatar.hash);
    } else {
        presence.setVCardUpdateType(QXmppPresence::VCardUpdateNotReady);
    }
    component()->sendPacket(presence);
}

void MucService::botLeaveMuc(const QString &mucJid)
{
    QXmppPresence presence;
    presence.setType(QXmppPresence::Unavailable);
    presence.setFrom(botFullJid());
    presence.setTo(mucJid % u'/' % BOT_MUC_NICK);
    presence.setMucSupported(true);
    if (!d->botAvatar.hash.isEmpty()) {
        presence.setVCardUpdateType(QXmppPresence::VCardUpdateValidPhoto);
        presence.setPhotoHash(d->botAvatar.hash);
    } else {
        presence.setVCardUpdateType(QXmppPresence::VCardUpdateNotReady);
    }
    component()->sendPacket(presence);
}

MucUser *MucService::findUser(const QString &jid)
{
    const QString fullJid = QXmppUtils::jidToBareJid(jid) % u'/' % USER_RESOURCE;
    for (auto &mucUsers : d->users) {
        for (auto &user : mucUsers) {
            if (fullJid == user.userJid) {
                return &user;
            }
        }
    }
    return nullptr;
}

QString MucService::localUserFullJid(QStringView user)
{
    return user % u'@' % component()->configuration().componentName() % u'/' % USER_RESOURCE;
}

QString MucService::usernameToMucNick(const QString &username)
{
    return username % USER_POSTFIX;
}

const QString &MucService::botJid()
{
    static const QString jid = BOT_USER % u'@' % component()->configuration().componentName();
    return jid;
}

const QString &MucService::botFullJid()
{
    static const QString jid = botJid() % u'/' % USER_RESOURCE;
    return jid;
}
