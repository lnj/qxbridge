// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.0-only OR LGPL-3.0-only

#pragma once

#include <variant>
#include <QString>

class QJsonValue;
class QVariant;

namespace QTgBot {

class ChatId
{
public:
    ChatId(qint64 id);
    ChatId(const QString &username);

    explicit operator QJsonValue() const;
    explicit operator QVariant() const;

private:
    std::variant<qint64, QString> m_id;
};

}
