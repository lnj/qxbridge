// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.0-only OR LGPL-3.0-only

#include "response.h"

#include <QJsonObject>

namespace QTgBot {

Response Response::fromJson(const QJsonObject &object)
{
    Response resp;
    resp.m_ok = object.value(QLatin1String("ok")).toBool();
    resp.m_result = object.value(QLatin1String("result"));
    resp.m_errorCode = object.value(QLatin1String("error_code")).toInt();
    resp.m_description = object.value(QLatin1String("description")).toString();
    return resp;
}

Response::Response()
{
}

bool Response::ok() const
{
    return m_ok;
}

void Response::setOk(bool ok)
{
    m_ok = ok;
}

QJsonValue Response::result() const
{
    return m_result;
}

void Response::setResult(const QJsonValue &result)
{
    m_result = result;
}

quint32 Response::errorCode() const
{
    return m_errorCode;
}

void Response::setErrorCode(quint32 errorCode)
{
    m_errorCode = errorCode;
}

QString Response::description() const
{
    return m_description;
}

void Response::setDescription(const QString &description)
{
    m_description = description;
}

}
