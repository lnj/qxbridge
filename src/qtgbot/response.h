// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.0-only OR LGPL-3.0-only

#pragma once

#include <QJsonValue>

namespace QTgBot {

class Response
{
public:
    static Response fromJson(const QJsonObject &object);

    Response();

    bool ok() const;
    void setOk(bool ok);

    QJsonValue result() const;
    void setResult(const QJsonValue &result);

    quint32 errorCode() const;
    void setErrorCode(quint32 errorCode);

    QString description() const;
    void setDescription(const QString &description);

private:
    bool m_ok = false;
    QJsonValue m_result;
    quint32 m_errorCode = 0;
    QString m_description;
};

}
