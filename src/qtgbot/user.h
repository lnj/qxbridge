// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.0-only OR LGPL-3.0-only

#pragma once

#include <optional>

#include <QString>
#include <QVector>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>

#include "qtgbotglobal.h"

namespace QTgBot {

class User
{
public:
    static User fromJson(const QJsonObject &);
    FROM_JSON_OVERLOADS(User)

    User();

    qint64 id() const;
    void setId(const qint64 &id);

    QString firstName() const;
    void setFirstName(const QString &firstName);

    QString lastName() const;
    void setLastName(const QString &lastName);

    QString username() const;
    void setUsername(const QString &username);

    QString languageCode() const;
    void setLanguageCode(const QString &languageCode);

    bool isBot() const;
    void setIsBot(bool isBot);

    bool canJoinGroups() const;
    void setCanJoinGroups(bool canJoinGroups);

    bool canReadAllGroupMessages() const;
    void setCanReadAllGroupMessages(bool canReadAllGroupMessages);

    bool supportsInlineQueries() const;
    void setSupportsInlineQueries(bool supportsInlineQueries);

private:
    qint64 m_id;
    QString m_firstName;
    QString m_lastName;
    QString m_username;
    QString m_languageCode;
    bool m_isBot;
    bool m_canJoinGroups;
    bool m_canReadAllGroupMessages;
    bool m_supportsInlineQueries;
};

}
